from flask import Flask, flash, redirect
from flask import jsonify
from flask import request
from flask import render_template
from flask_migrate import Migrate
from flask_wtf import CSRFProtect
from werkzeug.exceptions import NotFound
from sqlalchemy.exc import DatabaseError

from models import db
from views.products import products_app

app = Flask(__name__)
app.register_blueprint(products_app, url_prefix="/products")

app.config.update(
    ENV="development",
    SECRET_KEY="sfgsdgsdhshjwrywgxhsetsdfxg",
    SQLALCHEMY_DATABASE_URI="postgresql+psycopg2://username:passwd!@postgres:5432/blog",
    # SQLALCHEMY_ECHO=True,
)

CSRFProtect(app)
db.init_app(app)
migrate = Migrate(app, db)


@app.cli.command("db-create-all")
def db_create_all():
    print(db.metadata.tables)
    db.create_all()


def print_request():
    print("request", request)
    print("headers", request.headers)


@app.get("/", endpoint="index_page")
def get_root():
    return render_template("index.html")


@app.get("/args/")
def get_args():
    return {
        "request.args": request.args,
        "request.args['foo']": request.args["foo"],
        "request.args.get('foo')": request.args.get("foo"),
        "request.args.getlist('foo')": request.args.getlist("foo"),
        "request.args.getlist('spam')": request.args.getlist("spam"),
        "request.args.to_dict()": request.args.to_dict(),
        "request.args.to_dict('false')": request.args.to_dict(False),
        "message": "Hello!",
    }


@app.route("/hello/")
@app.route("/hello/<name>/")
def hello_world(name: str = None):
    # print_request()
    if name is None:
        name = request.args.get("name", "World")
    return f"<h1>Hello, {name}!</h1>"


@app.get("/items/")  # same @app.route("/items/")
def get_items():
    return {
        "items": [
            {"id": 1},
            {"id": 2},
        ],
    }


@app.get("/items_v1/")
def get_items_v1():
    return jsonify(None)


@app.get("/items_v2/")
def get_items_v2():
    return jsonify(spam="eggs", foo="bar")


@app.get("/items/<int:item_id>/")
def get_item(item_id: int):
    return {"item": {"id": item_id}}


@app.get("/items/<item_id>/")
def get_item_str(item_id: str):
    return {"item_id": item_id.upper()}


@app.errorhandler(DatabaseError)
def handle_databaseError(error):
    flash("oops! no db connection", "danger")
    return redirect("/")


@app.errorhandler(404)
def handle_404(error):
    if isinstance(error, NotFound) and error.description != NotFound.description:
        return error
    return render_template("404.html"), 404
